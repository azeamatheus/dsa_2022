# Problem 3

Marks: 30/100

## Problem

This problem focuses on a Restful API to manage a student record. A student is defined by a student number, a name, an email address and the courses the student took. For each course, there is the course code, the weights of the assessments in the course and the marks awarded for each assessment. The API should fulfil the following functions:

* Create a new student;
* Update student details;
* Update student's course details;
* lookup a single student;
* fetch all students;
* delete a student.

Note that the student number should serve as a unique identifier for a student.

Your task is to define the API following the OpenAPI standard and implement the client and service in the **Ballerina language**.

## Evaluation criteria

We will follow the criteria below to assess this problem:

* A correct description of the API in OpenAPI. (40%)
* Implementation of the restful client and service in the **Ballerina language**. (50%)
* Quality of design and implementation. (10%)

# Solution
// notes go here