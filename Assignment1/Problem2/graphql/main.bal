import ballerina/graphql;

public type covid19_stats record {|
    string date;
    readonly string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested; 
|};

configurable int port = 1111;
service /graphql on new graphql:Listener(port) {
     private  Covid19_stats stats;

    // invocations
    function init() {
        self.stats = new ("12/09/2021", "Khomas", 39, 465, 67, 1200);
    }
    // get all
    resource function get statistics() returns Covid19_stats {
        return self.stats;
    } 
    // get specified region
    // post new region statistics
    // update region statistics
    remote function updateStats(string date, int deaths, int confirmed_cases, int recoveries, int tested) returns Covid19_stats {
        self.stats.updateAllStats(date , deaths, confirmed_cases, recoveries, tested);
        return self.stats;
    }
    
}

public service class Covid19_stats {
    private string date;
    private string region;
    private int deaths;
    private int confirmed_cases;
    private int recoveries;
    private int tested;

    resource function get date() returns string {
        return self.date;
    }
    resource function get region() returns string {
        return self.region;
    }
    resource function get deaths() returns int {
        return self.deaths;
    }
    resource function get confirmed_cases() returns int {
        return self.confirmed_cases;
    }
    resource function get recoveries() returns int {
        return self.recoveries;
    }
    resource function get tested() returns int {
        return self.tested;
    }
    function init (string date, string region , int deaths, int confirmed_cases, int recoveries, int tested) {
        self.date = date;
        self.region = region;
        self.deaths = deaths;
        self.confirmed_cases = confirmed_cases;
        self. recoveries = recoveries;
        self.tested = tested;
    }
    function updateAllStats (string date , int deaths, int confirmed_cases, int recoveries, int tested) {
        self.date = date;
        self.deaths = deaths;
        self.confirmed_cases = confirmed_cases;
        self. recoveries = recoveries;
        self.tested = tested;
    }

}
