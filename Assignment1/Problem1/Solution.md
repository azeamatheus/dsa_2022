# Problem 1

Marks: 50/100

## Problem

In this problem, we wish to design and build components of an assessment management system using gRPC. The system has three user profiles: **learner**, **administrator** and **assessor**. Note that each user should have a unique identifier in the system. A learner registers for courses, submits assignments for each assessment of each course he/she registered for, and finally checks his/her result. An administrator adds new courses and sets the number of assessments for each course and the assessment weights. He/she also assigns assessors to each course. Finally, an assessor allocated for a course assesses the assignments submitted.

In short, we have the following operations:

* create_courses, where an administrator creates several courses, defines the number of assignments for each course and sets the weight for each assignment. This operation returns the code for each created course. It is bidirectional streaming;
* assign_courses, where an administrator assigns each created course to an assessor;
* create_users, where several users, each with a specific profile, are created. The users are streamed to the server, and the response is returned once the operation completes;
* submit_assignments, where a learner submits one or several assignments for one or multiple courses he/she registered for. The assignments are streamed to the server, and the response is received once the operation completes;
* request_assignments, where an assessor requests submitted assignments for a course he/she has been allocated. Note that an assignment can be marked only once. The function should stream back all assignments that have not been marked yet;
* submit_marks, where an assessor submits the marks for assignments;
* register, where a learner registers for one or several courses. All the courses are streamed to the server, and the result is returned once the operation completes;

Your task is to define a protocol buffer contract with the remote functions and implement both the client and the server in the **Ballerina Language**.

## Assessment Criteria

We will follow the criteria below to assess this problem:

* Definition of the remote interface in **Protocol Buffer**. (25%)
* Implementation of the gRPC client in the **Ballerina language**. (25%)
* Implementation of the gRPC server and server-side logic in response to the remote invocations in the **Ballerina Language**. (50%)

# Solution 
// info goes here